#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This file is part of Hugo (application launcher).
#
#    HUGO is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    HUGO is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with HUGO.  If not, see <http://www.gnu.org/licenses/>.
#
from PyQt4 import QtCore, QtGui
import os
import sys
import subprocess
import platform
import ConfigParser

os.environ['HUGO']=os.path.join(os.path.dirname(os.path.abspath(os.path.abspath(__file__))))
sys.path.append(os.path.normpath(os.environ['HUGO']))
os.environ['APP_TOOLS']= os.path.normpath(os.path.join(os.environ['HUGO'],"../app_tools_2.0"))

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.environ['HUGO'], 'defaults_%s.ini' % platform.system()))
os.environ["SERVER"] = config.get("network","storage")

from lib import util
from lib.gui import hugo_gui as gui

from pytools.launchers import maya
from pytools.launchers import nuke
from pytools.launchers import mudbox
from pytools.launchers import softimage

class Main(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        # This is always the same
        self.ui=gui.Ui_MainWindow()
        self.ui.setupUi(self)
        option = self.ui.retranslateUi(self)
        self.server = os.environ["SERVER"]
        self.job_list = util.find_folder(self.server,'job')
        self.seq_list = []
        self.shot_list = []
        self.ui.list_job.clear()
        self.ui.list_job.addItems(self.job_list)
        self.current_path = ""
        self.job = ""
        self.seq = ""
        self.shot = ""
        
        self.new_p_number()

    def new_p_number(self):
        self.job_list = util.find_folder(self.server, 'job')
        try:
            new_project_number = "%04d" % (int(self.job_list[len(self.job_list) - 1].split("_")[0]) + 1)
        except:
            new_project_number = "0001"
        self.ui.txt_new_project.setText(new_project_number + "_")

    def on_list_job_clicked(self):
        self.job = self.job_list[self.ui.list_job.currentRow()]
        self.shot, self.seq = "", ""
        self.ui.list_sequence.clear()
        self.ui.list_shot.clear()
        self.seq_list = util.find_folder(util.path_join(self.server, self.job, "06_vfx"), "seq")
        self.ui.list_sequence.addItems(self.seq_list)
        self.current_path = util.path_join(self.server, self.job)

        if len(self.seq_list) > 0:
            seqNumber = "%03d_" % (int(self.seq_list[(len(self.seq_list)-1)].split("_")[0]) + 1)
        else:
            seqNumber = "000_"
        self.ui.txtNew.setText(seqNumber)
        
    def on_list_sequence_clicked(self):
        self.ui.list_shot.clear()
        self.shot = ""
        self.job = self.job_list[self.ui.list_job.currentRow()]
        self.seq = self.seq_list[self.ui.list_sequence.currentRow()]
        self.shot_list = util.find_folder(util.path_join(self.server, self.job, "06_vfx", self.seq), "shot")
        self.ui.list_shot.addItems(self.shot_list)
        self.current_path = util.path_join(self.server, self.job, "06_vfx", self.seq)
        seqNumber = self.seq.split("_")[0]
        try:
            shotNumber = self.shot_list[len(self.shot_list)-1].split("_")[1].rstrip("0")
        except:
            shotNumber = "0"

        self.ui.txtNew.setText("%s_%s0_" % (seqNumber,"%02d" % (int(shotNumber)+1)))

    def on_list_shot_clicked(self):
        self.job = self.job_list[self.ui.list_job.currentRow()]
        self.seq = self.seq_list[self.ui.list_sequence.currentRow()]
        self.shot = self.shot_list[self.ui.list_shot.currentRow()]
        self.current_path = util.path_join(self.server, self.job, "06_vfx", self.seq,self.shot)
        os.environ['JOB']= self.job
        os.environ['SHOT']= self.current_path
        os.environ['SEQ']= self.seq
        os.environ['ASSETS']= util.path_join(self.server, self.job, 'library/3d_assets')
    
    def on_btn_mudbox_start_released(self):
        mudbox.start()
        
    def on_btn_maya_start_released(self):
        maya.start(util.path_join(self.server, self.job))

    def on_btn_softimage_start_released(self):
        softimage.start()

    def on_btn_nuke_start_released(self):
        nuke.start(util.path_join(self.server, self.job))

    def on_btn_renders3d_released(self):
        path = util.path_join(self.current_path, "3d/renders")
        self.open_folder(path)

    def on_btn_renders2d_released(self):
        path = util.path_join(self.current_path, "2d/renders")
        self.open_folder(path)     

    def on_btn_dailies_released(self):
        path = util.path_join(self.server, self.job, "05_dailies")
        self.open_folder(path)

    def on_btn_filemanager_released(self):
        self.open_folder(self.current_path)

    def open_folder(self, path):
        if platform.system() == "Linux":
            subprocess.Popen('nautilus --no-desktop --browser %s' % path, shell=True)
        if platform.system() == "Darwin":
            subprocess.Popen('open %s' % path, shell=True)
        if platform.system() == "Windows":
            subprocess.Popen('explorer %s' % path, shell=True)

    def on_btn_new_released(self):
        userInput = str(self.ui.txtNew.text()).replace(" ", "_")
        if self.seq == "":
            path = util.path_join(self.server, self.job, "06_vfx", userInput)
            os.makedirs(path)
            self.on_list_job_clicked()
        else:
            if self.shot == "":
                path = util.path_join(self.server, self.job, "06_vfx", self.seq, userInput)
                util.create_structure("shot", path)
                self.on_list_sequence_clicked()

    def on_btn_new_project_released(self):
        userInput = str(self.ui.txt_new_project.text()).replace(" ", "_")
        if not userInput == "":
            util.create_structure("job", userInput)
            self.ui.list_job.clear()
            self.ui.list_sequence.clear()
            self.ui.list_shot.clear()
            self.ui.txt_new_project.clear()
            self.job_list = util.find_folder(self.server, 'job')     
            self.ui.list_job.addItems(self.job_list)
            self.new_p_number()

   
def main():
    app = QtGui.QApplication(sys.argv)
    window=Main()
    window.show()
    sys.exit(app.exec_())
if __name__ == "__main__":
    main()
    
        


