#!/usr/bin/env python
import os
import platform
import subprocess
from lib import util

import ConfigParser

def maya_set_environment(path):
    '''
    append project environment to the startup
    '''
    util.append_env('MAYA_PLUG_IN_PATH', util.path_join( path, 'plugins'))
    util.append_env('MAYA_SCRIPT_PATH', util.path_join( path, 'scripts'))
    util.append_env('MAYA_MODULE_PATH', util.path_join( path, 'modules'))
    util.append_env('XBMLANGPATH', util.path_join( path, 'icons'))
    util.append_env('PATH', util.path_join( path, 'plugins'))
    util.append_env('PYTHONPATH', util.path_join( path, 'scripts'))
    util.append_env('MI_CUSTOM_SHADER_PATH', util.path_join( path, 'mentalray/lib'))
    util.append_env('MI_CUSTOM_SHADER_PATH', util.path_join( path, 'mentalray/include'))
    util.append_env('MAYA_SHELF_PATH', util.path_join( path, 'shelves')) #suggestion from Mark
    return 1

def start(job):
    config = ConfigParser.ConfigParser()
    config.read(os.path.join(os.environ['HUGO'], 'defaults_%s.ini' % platform.system()))
    maya_bin = config.get("applications","maya_bin")
    maya_set_environment(util.path_join(os.environ["APP_TOOLS"], "maya"))
    maya_set_environment(util.path_join(job, "04_library/tools/maya"))
    try:
        subprocess.Popen("%s -proj %s" %( maya_bin, util.path_join(os.environ["SHOT"], "3d")), shell=True)
    except:
        subprocess.Popen(maya_bin, shell=True)