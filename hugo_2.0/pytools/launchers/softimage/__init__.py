#!/usr/bin/env python
import os
import platform
import subprocess
from lib import util

import ConfigParser


def workgroups():
    """
    global workgroups and the shot workgroup
    """
    shot_workgroup = util.path_join(os.environ["SERVER"], os.environ["JOB"], "04_library/tools/softimage")
    base_workgroup = util.path_join(os.environ["APP_TOOLS"], "softimage/base")
    implosia_workgroup = util.path_join(os.environ["APP_TOOLS"], "softimage/ImplosiaFX_workgroup")
    return "%s;%s;%s" % (base_workgroup, shot_workgroup, implosia_workgroup)

def create_shotlist():
    """
    creates a .xsiprojects which Softimage uses to read in 
    all the shots as a list.
    """
    xsi_projects_file = util.path_join(os.environ["SERVER"], os.environ["JOB"], '04_library/tools/softimage/Data' , '%s.xsiprojects' % os.environ["SEQ"])
    shots = util.find_folder(util.path_join(os.environ["SERVER"], os.environ["JOB"], "06_vfx", os.environ["SEQ"]), "shot")      
    shot_paths = ["%s, %s\n" % (util.path_join(os.environ["SERVER"], os.environ["JOB"], "06_vfx", os.environ["SEQ"], shot, "3d"), shot) for shot in shots]
    shot_paths.sort()
    if os.path.exists(xsi_projects_file):
        os.remove(xsi_projects_file)
    create_xsi_projects_file = open(xsi_projects_file, 'w')
    [create_xsi_projects_file.write(path) for path in shot_paths]       
    create_xsi_projects_file.close()

def start():
    config = ConfigParser.ConfigParser()
    config.read(os.path.join(os.environ['HUGO'], 'defaults.ini'))
    softimage_bin = config.get("applications","softimage_bin")
    softimage_batch = config.get("applications","softimage_batch")
    xsi_projects_file = create_shotlist()
    subprocess.Popen( '%s -processing -w "%s" && %s' % ( softimage_batch, workgroups(), softimage_bin ), shell=True )

