#!/usr/bin/env python
import os
import platform
import subprocess
from lib import util

import ConfigParser

def mudbox_environment():
	util.append_env('MUDBOX_DATA_PATH', util.path_join( os.environ["SHOT"], '3d/data'))
	return 0

def start():
	config = ConfigParser.ConfigParser()
	config.read(util.path_join(os.environ['HUGO'], 'defaults.ini'))
	mudbox_bin = config.get("applications","mudbox_bin")
	mudbox_environment()
	os.chdir(util.path_join(os.environ["SHOT"],"3d/scenes"))
	subprocess.Popen(mudbox_bin, shell=True)

