#!/usr/bin/env python
import os
import subprocess
import platform
from lib import util

import ConfigParser

def set_nuke_env(job):
    util.append_env( 'NUKE_PATH', util.path_join(os.environ["APP_TOOLS"],"nuke") )
    util.append_env( 'NUKE_PATH', "%s/04_library/tools/nuke" % (job))
    os.chdir(util.path_join(os.environ["SHOT"],"2d/scenes/comp"))

def start(job):
    config = ConfigParser.ConfigParser()
    config.read(util.path_join(os.environ['HUGO'], 'defaults.ini'))
    nuke_bin = config.get("applications","nuke_bin")
    set_nuke_env(job)
    subprocess.Popen("%s --nukex" % nuke_bin, shell=True)
    
