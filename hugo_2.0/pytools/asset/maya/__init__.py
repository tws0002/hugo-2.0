import os
import shutil
import fileinput
import subprocess
from pymel.core import *

import sys
sys.path.append(os.environ["HUGO"])
import pytools.asset as asset

class SelectionInfo():
    """
    Class to list connections in the current selection
    
    USAGE:
    import pytools.asset.maya
    SelectionInfo = pytools.asset.maya.SelectionInfo() 

    SelectionInfo.shadingGroups(maya.cmds.ls(sl=True))  # returns ShadingGroups under the selection
    SelectionInfo.shaders(maya.cmds.ls(sl=True))        # returns shaders under the selection
    SelectionInfo.textures(maya.cmds.ls(sl=True))       # returns textures under the selection
    """

    def shaders(self, TopNode): 
        """
        # Main function in the AssetConnections Class
        # Returns a list of the shaders in a selection
        """
        shaders = []
        for geo in listRelatives(TopNode):
            try:
                for shd in geo.getShape().outputs(type='shadingEngine'):
                    shaders.append(shd)
                    print shd
            except:
                pass
        return set(shaders)

    def textures(self, TopNode):
        """
        Returns a list of the textures in a selection
        """
        texture_files = []
        for shd in self.shaders(TopNode):
            for img in ls(listHistory(shd, type="file")):
                texture_files.append(getAttr(img.fileTextureName))
        return set(texture_files)

    def shadingGroups(self, TopNode):
        """
        Returns a dictionary with shaders and geometry that are assosiated with 
        the shader
        """   
        shader_dict = {}
        for geo in self.shaders(TopNode):
            shader_dict[geo]= [obj.name() for obj in geo.members()]
        return shader_dict

def convertTextures(TopNode):
    """
    covert all texture files to linear exr
    """

    texture_files = connectedTextures(TopNode)
    for img in texture_files:
        exr_file = "%s.exr" % os.path.splitext(img)[0]
        subprocess.Popen("%s -v -i -p %s " % (oiio, img, exr_file), shell=True)

def assetfileCleanup():
    """
    Clean up the exported maya file and replace all the file nodes
    to be relative to the assetfileCleanup
    """
    print "parse through maya file and fix file paths"

def assetfileInfo(TopNode):
    """
    information about asset
    """
    shader_dict = connectedShaders(TopNode)
    project = os.environ["PROJECT"]
    topnode = TopNode
    origin = sceneName()

class MayaAssetExport():
    def export(TopNode):
        """
        exports asset to correct location
        """
        assetId = assets.next_id()
        assetName = TopNode
        assetPath = os.path.join(os.environ["ASSETS"], "%s_%s" % (assetId,assetName))
        for folder in ["shaders","textures"]:
            try:
                os.makedirs(os.path.join(assetPath, folder), 0755) 
            except:
                pass
        exportSelected("%s/model.ma" % assetPath, type="mayaAscii", pr=True, f=True, s=False)
        exportAssetShaders(TopNode, assetPath)

    def exportShaders(TopNode, assetPath):
        '''
        export shaders that are connected to the asset and change the file input path
        '''
        shaders = connectedShaders(TopNode)
        print shaders
        select(shaders, r=True)
        exportSelected("%s/shaders/shaders.ma" % assetPath, type="mayaAscii", pr=True, f=True)

        for img in connectedTextures(TopNode):
            asset_img = "%s/textures/%s.exr" % (assetPath, os.path.basename(os.path.splitext(img)[0]))
            subprocess.Popen("maketx %s --resize -d half -o %s" % (img,asset_img),shell=True)
            for mayafile in ["model.ma","shaders/shaders.ma"]:
                for line in fileinput.FileInput("%s/%s" % (assetPath,mayafile), inplace=1):
                    if img in line:
                        line = line.replace(img, "textures/%s.exr" % os.path.basename(os.path.splitext(img)[0]))
                    print line,
     

