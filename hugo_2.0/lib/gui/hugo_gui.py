from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(755, 600)
        MainWindow.setMinimumSize(QtCore.QSize(755, 450))
        MainWindow.setMaximumSize(QtCore.QSize(755, 450))
        font = QtGui.QFont()
        font.setPointSize(8)
        font.setItalic(True)
        MainWindow.setFont(font)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 731, 301))
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))

        self.label_project = QtGui.QLabel(self.layoutWidget)
        self.label_project.setAlignment(QtCore.Qt.AlignCenter)
        self.label_project.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label_project)

        self.label_sequence = QtGui.QLabel(self.layoutWidget)
        self.label_sequence.setAlignment(QtCore.Qt.AlignCenter)
        self.label_sequence.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_sequence)

        self.label_shot = QtGui.QLabel(self.layoutWidget)
        self.label_shot.setAlignment(QtCore.Qt.AlignCenter)
        self.label_shot.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_shot)

        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.list_job = QtGui.QListWidget(self.layoutWidget)
        self.list_job.setObjectName(_fromUtf8("list_job"))
        self.horizontalLayout.addWidget(self.list_job)
        self.list_sequence = QtGui.QListWidget(self.layoutWidget)
        self.list_sequence.setObjectName(_fromUtf8("list_sequence"))
        self.horizontalLayout.addWidget(self.list_sequence)
        self.list_shot = QtGui.QListWidget(self.layoutWidget)
        self.list_shot.setObjectName(_fromUtf8("list_shot"))
        self.horizontalLayout.addWidget(self.list_shot)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setEnabled(True)
        self.groupBox.setGeometry(QtCore.QRect(10, 320, 731, 141))
        self.groupBox.setMouseTracking(True)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))

        #Application buttons Buttons
        
        self.btn_maya_start = QtGui.QToolButton(self.groupBox)
        self.btn_maya_start.setObjectName(_fromUtf8("btn_maya_start"))
        self.btn_maya_start.setGeometry(QtCore.QRect(10, 31, 64, 32))

        self.btn_softimage_start = QtGui.QToolButton(self.groupBox)
        self.btn_softimage_start.setObjectName(_fromUtf8("btn_softimage_start"))
        self.btn_softimage_start.setGeometry(QtCore.QRect(10, 62, 64, 32))

        self.btn_mudbox_start = QtGui.QToolButton(self.groupBox)
        self.btn_mudbox_start.setObjectName(_fromUtf8("btn_mudbox_start"))
        self.btn_mudbox_start.setGeometry(QtCore.QRect(80, 31, 64, 64))

        self.btn_nuke_start = QtGui.QToolButton(self.groupBox)
        self.btn_nuke_start.setObjectName(_fromUtf8("btn_nuke_start"))
        self.btn_nuke_start.setGeometry(QtCore.QRect(150, 31, 64, 64))

        self.btn_dailies = QtGui.QToolButton(self.groupBox)
        self.btn_dailies.setObjectName(_fromUtf8("btn_dailies"))
        self.btn_dailies.setGeometry(QtCore.QRect(220, 31, 64, 64))
        
        self.btn_renders3d = QtGui.QToolButton(self.groupBox)
        self.btn_renders3d.setObjectName(_fromUtf8("btn_renders3d"))
        self.btn_renders3d.setGeometry(QtCore.QRect(290, 31, 64, 32))

        self.btn_renders2d = QtGui.QToolButton(self.groupBox)
        self.btn_renders2d.setObjectName(_fromUtf8("btn_renders2d"))
        self.btn_renders2d.setGeometry(QtCore.QRect(290, 62, 64, 32))

        self.btn_filemanager = QtGui.QToolButton(self.groupBox)
        self.btn_filemanager.setObjectName(_fromUtf8("btn_filemanager"))
        self.btn_filemanager.setGeometry(QtCore.QRect(360, 31, 64, 64))        
     

        self.txtNew = QtGui.QLineEdit(self.groupBox)
        self.txtNew.setObjectName("txtSequence")
        self.txtNew.setGeometry(QtCore.QRect(430, 31, 150, 30))

        self.btn_new = QtGui.QToolButton(self.groupBox)
        self.btn_new.setGeometry(QtCore.QRect(580, 31, 100, 30))
        self.btn_new.setObjectName(_fromUtf8("btn_new"))


        self.txt_new_project = QtGui.QLineEdit(self.groupBox)
        self.txt_new_project.setObjectName("txt_new_project")
        self.txt_new_project.setGeometry(QtCore.QRect(430, 62, 150, 30))

        self.btn_new_project = QtGui.QToolButton(self.groupBox)
        self.btn_new_project.setGeometry(QtCore.QRect(580, 62, 100, 30))
        self.btn_new_project.setObjectName(_fromUtf8("btn_new_project"))
        

        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):

        MainWindow.setWindowTitle(_translate("MainWindow", "Hugo", None))
        self.label_project.setText(_translate("MainWindow", "Project", None))
        self.label_sequence.setText(_translate("MainWindow", "sequence", None))
        self.label_shot.setText(_translate("MainWindow", "shot", None))
        self.btn_filemanager.setText(_translate("MainWindow", "Folder", None))
        self.btn_dailies.setText(_translate("MainWindow", "Dailies", None))
        self.btn_maya_start.setText(_translate("MainWindow", "Maya", None))
        self.btn_softimage_start.setText(_translate("MainWindow", "Softimage", None))
        self.btn_nuke_start.setText(_translate("MainWindow", "Nuke", None))
        self.btn_mudbox_start.setText(_translate("MainWindow", "Mudbox", None))
        self.btn_renders3d.setText(_translate("MainWindow", "Renders 3D", None))
        self.btn_renders2d.setText(_translate("MainWindow", "Renders 2D", None))

        self.btn_new.setText(_translate("MainWindow", "NEW shot/seq", None))
        self.btn_new_project.setText(_translate("MainWindow", "NEW Project", None))

