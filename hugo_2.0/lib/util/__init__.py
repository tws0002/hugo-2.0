#!/usr/bin/env python
import os
import platform
import re
import shutil
import xml.etree.ElementTree as ET

def path_join(*args):
    if platform.system() == "Windows":
        path = os.path.join(*args).replace("/","\\")
    else:
        path = os.path.join(*args)
    return path

def find_folder(path, c):
    crit = {"job":"[0-9]{1,4}_.+","seq":"[0-9]{1,3}_.+","shot":"[0-9]{1,3}_.+"}
    FOLDERS = [F for F in os.listdir(path) if re.match(crit[c], F) and os.path.isdir(path_join(path, F))]
    FOLDERS.sort()
    return FOLDERS

def find_assets():
    path = os.environ['ASSETS']
    FOLDERS = [F for F in os.listdir(path) if re.match("[0-9]{1,4}_.+", F) and os.path.isdir(os.path.join(path, F))]
    FOLDERS.sort()
    return FOLDERS


def append_env(env, value):
    if platform.system() == "Windows":
        separator = ";"
    else:
        separator = ";"
    if os.environ.has_key(env):
        value = os.environ[env] + separator + value
        os.environ[env] = value # I added this line. 
        print env, value
    else:
        os.environ[env] = value
        print env, value
  
def create_structure(dir_type, name):
    tree = ET.parse(os.path.join(os.environ["HUGO"],"lib/util/project_tree.xml"))
    root = tree.getroot()
    folders = root.findall(dir_type)
    make_folders(folders, "", dir_type, name)

def make_folders(folders, basedir, dir_type, name):
    print folders, basedir, dir_type, name
    for i, each in enumerate(folders):
        folder_path = os.path.join(basedir, each.get('name').replace(dir_type, name))
        try:
            os.makedirs(os.path.join(os.environ["SERVER"], folder_path), 0755)
        except:
            print "Failed to create: %s" % folder_path
            pass
        make_folders(each, folder_path, 'dir', name)
    if dir_type == "shot":
        shutil.copy("%s/workspace.mel" % (path_join(os.environ['HUGO'], "lib/util")), "%s/workspace.mel" % (path_join(folder_path, "3d")))         
        shutil.copy("%s/dsprojectinfo" % (path_join(os.environ['HUGO'], "lib/util")), "%s/dsprojectinfo" % (path_join(folder_path, "3d/system")))
    return





