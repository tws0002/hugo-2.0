Hugo is a application launcher and project creator

INSTALLATION
minimum specs, Python 2.6 PyQt 4.6
Tested on Windows7 and CentOS 6.4

1. unpack or place the folder "hugo_2.0" and "app_tools_2.0" in the same location. 
2. edit the defaults_(platform).ini to suite your installations

"storage" is the location of the place where you want to save your projects. This can be on a network or local.

=============What does it do?===================

1.) Creates project structure for Post-Production-VFX.
		Grading
		Offline (edit & timeline)
		Producers
		3D/VFX
		
2.) Sets shot environment for Softimage, Maya and Nuke
	Enables you to have project specific tools that are
	dynamicaly loaded on each start.
	Switch projects and you will also switch tools.
	
	Current environment variables that sets
	
	Global Envs.
	HUGO 		= location of hugo.py
	APP_TOOLS 	= location of the folder that has global tools
	JOB 		= name of the project
	SEQ 		= name of the sequence that you are in
	SHOT 		= name of the shot that you are in
	
	Depending on application there might be more variables that are being set. 
	
	[HUGO]/pytools/launchers for each application and what envs that are set

