import os
import sys
sys.path.append(os.environ["HUGO"])
from win32com.client import constants as C
from win32com.client import Dispatch
si = Dispatch('XSI.Application')

import pytools.asset
from pytools.asset.softimage import asset_util
from pytools.asset.softimage import asset_export
reload(asset_util)
reload(asset_export)

def XSILoadPlugin( in_reg ):
    in_reg.Author = "stefan"
    in_reg.Name = "AssetExport_UIPlugin"
    in_reg.Major = 1
    in_reg.Minor = 0
    in_reg.RegisterProperty("AssetExport_UI")
    in_reg.RegisterMenu(C.siMenu3DViewObjectContextID,"AssetExport_UI_Menu",False,False)
    #RegistrationInsertionPoint - do not remove this line

    return True

def XSIUnloadPlugin( in_reg ):
    strPluginName = in_reg.Name
    si.LogMessage(str(strPluginName) + str(" has been unloaded."),C.siVerbose)
    return True

def get_asset_id():
    assets = pytools.asset.find_assets()
    if not assets == []:
        asset_id = int(assets[len(assets)-1].split("_")[0])+1
    else:
        asset_id = 1
    return "%04d" % asset_id

def AssetExport_UI_Define( in_ctxt ):
    try:
        SelectionName = si.Selection(0).Name.split("_")[0]
    except:
        SelectionName = si.Selection(0).Name
    prop = in_ctxt.Source
    prop.AddParameter2("asset_id", C.siString, get_asset_id(),None,None,None,None,C.siClassifMetaData, 0, "Asset ID")
    prop.AddParameter2("asset_name", C.siString, SelectionName ,None,None,None,None,C.siClassifMetaData, 0, "Asset Name")
    prop.AddParameter2("asset_type", C.siString, "geo" ,None,None,None,None,C.siClassifMetaData, 0, "Asset Type")
    prop.AddParameter2("mia", C.siBool, 0 ,None,None,None,None,C.siClassifMetaData, 0, "create mia file")
    return True

def AssetExport_UI_OnInit( ):
    ui = PPG.PPGLayout
    ui.Clear()
    ui.AddGroup("Asset Information",True, 100)
    ui.AddGroup("",False, 100)
    ui.AddItem("asset_name", "Name")
    ui.AddItem("asset_id", "ID")
    ui.AddEnumControl("asset_type", pytools.asset.asset_types(), "Type")
    ui.EndGroup()
    ui.AddGroup("",False, 100)
    ui.AddSpacer(20,20)
    ui.AddItem("mia", "create mia file")
    ui.AddSpacer(20,20)
    ui.AddRow()
    item = ui.AddButton("AssetExport","Export Asset")
    item.SetAttribute( C.siUICX, 150 )
    item.SetAttribute( C.siUICY, 50 )
    item = ui.AddButton("AssetUpdate","Update Asset")
    item.SetAttribute( C.siUICX, 150 )
    item.SetAttribute( C.siUICY, 50 )
    ui.EndRow()
    ui.EndGroup()
    ui.EndGroup()
    PPG.Refresh()

def AssetExport_UI_AssetExport_OnClicked( ):
    """
    export the asset
    """
    asset_export.export_Model(PPG.asset_id.Value, PPG.asset_name.Value, PPG.asset_type.Value, 0, PPG.mia.Value)

def AssetExport_UI_AssetUpdate_OnClicked( ):
    """
    export the asset
    """
    AssetInfo = si.GetValue("%s.AssetInfo.text" % si.Selection(0))
    asset_id = AssetInfo.split("\n")[0].split(":")[1]
    asset_name = AssetInfo.split("\n")[1].split(":")[1]
    asset_type = AssetInfo.split("\n")[2].split(":")[1]
    asset_export.export_Model(asset_id, asset_name, asset_type,1,1)


def AssetExport_UI_OnClosed( ):
    si.DeleteObj("AssetExport_UI")

def OnAssetExport_UIMenuClicked( in_ctxt ):
    si.AddProp("AssetExport_UI")
    return True


